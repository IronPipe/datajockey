# README #

__dataJockey__ is a simple Python script to convet Revlo points into PhantomBot points.  This creates a 1 to 1 copy of your Revlo points into PhantomBot, no appending or multiple posting of viewers to your database.
 
Instructions and code are included for a browser addon [Webscraper](http://webscraper.io/) 
to download your Revlo points.

## While dataJockey has been tested, data corruption is always possible.  Please backup your phantombot.db file before using this program!!!!

### How do I get set up? ###

* Download these files - Downloads -> Download repository
* Right click -> Extract all
* Get [Webscraper](http://webscraper.io/) for your browser

### Scrape your Revlo data

#### For best results scrape Revlo when your stream is offline and points are not changing
* Open your browser to your Revlo current leaderboard https://revlo.co/username/dashboard/leaderboard/current, replacing username with your Revlo account name
* Press F12 to open your browser's developer's tools.  Click the right most tab of developer's tools (Web Scraper) *Note: having dev tools at the bottom of the browser is advised to ensure easy scraping*
* Create new sitemap -> Import sitemap
* Copy the text from *revloScrape.txt* (found in your dataJockey folder) and paste that into Sitemap JSON for your scraper
* Rename Sitemap must be set to **revlopoints** then click Import Sitemap button
* Click Sitemap (revlopoints) -> Edit metadata and set Start URL to `https://www.revlo.co/username/dashboard/leaderboard/current` replacing username with your Revlo account name.  Make sure Sitemap name is revlopoints then click Save Sitemap
* Click Sitemap (revlopoints) -> Scrape -> Start Scraping.  A new window will open and scrape your points there
* Once the window closes you'll get a notification the scraping is done and see some of the data collected.  Click Sitemap (revlopoints) -> Export data as CSV -> Download now! link

### Transfer data into your PhantomBot database ###

#### PhantomBot should NOT be running when you transfer the points, please shut it down for this part
* Open your **dataJockey** folder and double click *dataJockey.exe*
* revlopoints.csv and phantombot.db locations can be easily pasted by right clicking on the file -> Properties -> highlight Location data -> Control + C and then Control + V in the dataJockey console window
* Press Enter to close the console window.  PhantomBot is ready to launch with all your points transfered.